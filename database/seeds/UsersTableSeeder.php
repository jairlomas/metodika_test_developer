<?php

use Illuminate\Database\Seeder;

use MetodikaTI\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demon = array(
            'name' => 'Admin',
            'email' => 'admin@metodika.mx',
            'password' => bcrypt('admin'),
            'user_profile_id' => 1,
        );
        User::create($demon);
    }
}
