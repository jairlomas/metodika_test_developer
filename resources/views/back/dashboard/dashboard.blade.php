@extends('back.layout.dashboard')

@section('CSS')
    <!-- chartist CSS -->
    <link href="{{ asset('assets/node_modules/morrisjs/morris.css') }}" rel="stylesheet">
@endsection

{{-- Page Title --}}
@section('pageTitle')
    Dashboard
@stop

{{-- Content Title --}}
@section('contentTitle')
    Dashboard
@stop

@section('content_dashboard')

    <!-- Info box -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-success"><i class="ti-user"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h3 class="m-b-0">
                                {{--{{ $app_users->count() }}--}}
                            </h3>
                            <h5 class="text-muted m-b-0">Clientes registrados</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-info"><i class="ti-bookmark-alt"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h3 class="m-b-0">
                                {{--{{ $orders_pendientes->count() }}--}}
                            </h3>
                            <h5 class="text-muted m-b-0">Ordenes pendientes de atender</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-4 col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row">
                        <div class="round align-self-center round-danger"><i class="ti-bookmark-alt"></i></div>
                        <div class="m-l-10 align-self-center">
                            <h3 class="m-b-0">
                                {{--{{ $orders_terminado->count() }}--}}
                            </h3>
                            <h5 class="text-muted m-b-0">Ordenes finalizadas</h5></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- New Customers List and New Products List -->
    <!-- ============================================================== -->
    <!-- /row -->
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Ultimos ordenes recibidas</h5>
                    <p class="text-muted">Ordenes pendientes de enviar</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Orden</th>
                                <th>Tipo de pago</th>
                                <th>Total</th>
                                <th>Estatus de pago</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($orders_pendientes->get() as $item)--}}
                                {{--<tr>--}}
                                    {{--<td>{{ $item->order_id  }}</td>--}}
                                    {{--<td>{{ $item->payment_type() }}</td>--}}
                                    {{--<td>{{ $item->total  }}</td>--}}
                                    {{--<td>{{ $item->payment_status()  }}</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">Ultimos ordenes finalizadas</h5>
                    <p class="text-muted">Ordenes ya enviadas</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Orden</th>
                                <th>Tipo de pago</th>
                                <th>Total</th>
                                <th>Estatus de pago</th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($orders_terminado->limit(10)->get() as $item)--}}
                                {{--<tr>--}}
                                    {{--<td>{{ $item->order_id  }}</td>--}}
                                    {{--<td>{{ $item->payment_type() }}</td>--}}
                                    {{--<td>{{ $item->total  }}</td>--}}
                                    {{--<td>{{ $item->payment_status()  }}</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- ============================================================== -->
    <!-- End Page Content -->

@endsection

@section('JS')
{{--    <script src="{{ asset('assets/node_modules/morrisjs/morris.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>--}}
    <!-- Chart JS -->
{{--    <script src="{{ url('template/dist/js/dashboard1.js') }}"></script>--}}
@endsection
