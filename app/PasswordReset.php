<?php

namespace MetodikaTI;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    protected $table = "password_resets";

}
