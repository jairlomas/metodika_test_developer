<?php

namespace MetodikaTI\Imports;

use MetodikaTI\PostalCode;
use Maatwebsite\Excel\Concerns\ToModel;

class PostalCodeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        if($row[0] != 'CP') {
            if($row[0] != ""){
                return new PostalCode([
                    'cp' => trim($row[0]),
                    'colony' => trim($row[1]),
                    'municipaly' => trim($row[2]),
                    'city' => trim($row[3]),
                    'state' => trim($row[4])
                ]);
            }
        }
    }
}
