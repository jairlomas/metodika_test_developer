<?php namespace MetodikaTI\Library;

use Auth;
use Conekta\Conekta;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Mail;
use MetodikaTI\DegreeFrame;
use MetodikaTI\DegreeFrameConfiguration;
use MetodikaTI\DiplomaFrame;
use MetodikaTI\DiplomaFrameConfiguration;
use MetodikaTI\Extra;
use MetodikaTI\Glass;
use MetodikaTI\Order;
use MetodikaTI\PortaretratoFrame;
use MetodikaTI\PortaretratoFrameConfiguration;
use MetodikaTI\Reservation;
use MetodikaTI\SystemModule;
use MetodikaTI\University;
use MetodikaTI\User;
use Permission;
use Session;


class Pastora {


    public static function in_relation($object, $field_id, $field) {
        if (!is_null($object)) {
            if (!is_null($object->$field_id)) {
                if (!is_null($object->$field_id->$field)) {
                    return  true;
                }
            }
        }
        return false;
    }
    /**
     * Metodo que devuelve verdadero si una propiedad se encuentra en un arreglo
     *
     * @return Boolean
     */
    public static function in_object($value,$object) {
        if (is_object($object)) {
            foreach($object as $key => $item) {
                if ($value==$key) return true;
            }
        }
        return false;
    }

    /**
     * Metodo que devuelve los permisos que tiene el usuario
     *
     * @return JSON
     */
    public static function userProfile()
    {

        return User::find(Auth::user()->id)->userProfile->permits;
    }

    /**
     * Metodo que convierte un Json a Array
     *
     * @return Array
     */
    public static function jsonToArray($json)
    {
        $json = (array)json_decode($json);
        $response = [];
        foreach ($json as $key => $value) {
            $response[(int)$key] = (int)$value;
        }
        return $response;
    }

    /**
     * This method convert the array to object
     *
     * @param $array array to convert to object
     * @return mixed return the array on object
     */
    public static function arrayToObject($array){
        return json_decode(json_encode($array));
    }


    /**
     *
     *
     */
    public function strongMeter($password)
    {
        $rank = 0;
        $rank += (strlen(trim($password)) > 7) ? 5 : 0;
        $rank += (strlen(trim($password)) > 11 && strlen(trim($password)) < 16) ? 10 : 0;
        $rank += (preg_match("/([A-Z])/", $password)) ? 10 : 0;
        $rank += (preg_match("/([0-9])/", $password)) ? 10 : 0;
    }

    /**
     * Metodo que construye el arbol de modulos del sistema
     *
     * @var $parent integer. ID del papa del módulo
     *
     * @return array
     */
    public static function moduleTree($parent = 0)
    {

        $response = [];

        foreach (SystemModule::where('parent', '=', $parent)->orderBy('order')->get() as $module) {

            $response[] = [
                'id'    => $module->id,
                'name'  => $module->name,
                'url'   => $module->url,
                'icon'  => $module->icon,
                'parent_as_child' => $module->parent_as_child,
                'child' => self::moduleTree($module->id)
            ];

        }

        return $response;

    }


    /**
     *
     *
     */
    public static function cleanPhone($phone)
    {
        $mapper = [
            '/',
            '(',
            ')',
            '-',
            ' '
        ];

        foreach ($mapper as $key => $value) {

            $phone = str_replace($value, "", $phone);

        }

        return $phone;
    }

    public static function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); //turn the array into a string
    }

    public function getRandomWord($len = 5) {
        $word = array_merge(range('0', '9'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }





    //PAYMENTS CONEKTA
    public static function create_customer($name, $email, $phone, $token_id){

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";


        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        try {

            $customer = \Conekta\Customer::create(
                array(
                    "name" => $name,
                    "email" => $email,
                    "phone" => $phone,
                )//customer
            );

            $response["error"] = false;
            $response["message"] = "";
            $response["customer"] = $customer;

        } catch (\Conekta\ProccessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function add_card($customer_id, $token_id){

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";


        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        try {

            $customer = \Conekta\Customer::find($customer_id);
            $source = $customer->createPaymentSource(array(
                'token_id' => $token_id,
                'type'     => 'card'
            ));

            $response["error"] = false;
            $response["message"] = "";
            $response["customer"] = $customer;

        } catch (\Conekta\ProccessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function delete_card($customer_id, $card_id){

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";


        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        try {

            $customer = \Conekta\Customer::find($customer_id);
            for ($i = 0; $i < count($customer->payment_sources); $i++){
                if( $customer->payment_sources[$i]["id"] == $card_id ){
                    $source = $customer->payment_sources[$i]->delete();
                    $i = count($customer->payment_sources) + 1;
                }
            }

            $response["error"] = false;
            $response["message"] = "";
            $response["customer"] = $source;

        } catch (\Conekta\ProccessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function get_user_from_conekta($customer_id){

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";


        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        try {

            $customer = \Conekta\Customer::find($customer_id);

            $response["error"] = false;
            $response["message"] = "";
            $response["customer"] = $customer;

        } catch (\Conekta\ProccessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function make_payment($concepto, $cantidad_cobrar, $app_user, $card_id, $address_to_send){

        $cantidad_cobrar = ($cantidad_cobrar * 100);
        $cantidad_cobrar = explode(".", $cantidad_cobrar);

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";

        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");

        try{
            $order = \Conekta\Order::create(
                array(
                    "line_items" => array(
                        array(
                            "name" => $concepto,
                            "unit_price" => $cantidad_cobrar[0],
                            "quantity" => 1
                        )
                    ),
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $app_user->customer_id
                    ), //customer_info
                    "charges" => array(
                        array(
                            "payment_method" => array(
                                "type" => "card",
                                "payment_source_id" => $card_id,
                            )
                        )
                    ),
                    "shipping_lines" => array(
                        array(
                            "amount" => 0,
                            "carrier" => "Fedex",
                            "method" => "Terrestre",
                            "tracking_number" => "----",
                            "object" => "shipping_line",
                            "id" => "",
                            "parent_id" => ""
                        )
                    ),
                    "shipping_contact" => array(
                        "id" => "",
                        "object" => "shipping_contact",
                        "created_at" => "",
                        "parent_id" => "",
                        "receiver" => $app_user->name,
                        "phone" => $app_user->phone,
                        "between_streets" => $address_to_send->address,
                        "address"=> array(
                            "street1" => $address_to_send->address,
                            "city" => $address_to_send->city,
                            "state" => $address_to_send->state,
                            "postal_code" => $address_to_send->cp,
                            "country" => "mx",
                            "residential" => true,
                            "object" => "shipping_address"
                        )
                    )
                )
            );


            $response["error"] = false;
            $response["message"] = "";
            $response["order"] = $order;

        } catch (\Conekta\ProcessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function make_payment_oxxo($concepto, $cantidad_cobrar, $app_user, $address_to_send){

        $cantidad_cobrar = ($cantidad_cobrar * 100);
        $cantidad_cobrar = explode(".", $cantidad_cobrar);

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";

        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");

        try{
            $order = \Conekta\Order::create(
                array(
                    "line_items" => array(
                        array(
                            "name" => $concepto,
                            "unit_price" => $cantidad_cobrar[0],
                            "quantity" => 1
                        )
                    ),
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $app_user->customer_id
                    ), //customer_info
                    "charges" => array(
                        array(
                            "payment_method" => array(
                                "type" => "oxxo_cash"
                            )
                        )
                    ),
                    "shipping_lines" => array(
                        array(
                            "amount" => 0,
                            "carrier" => "Fedex",
                            "method" => "Terrestre",
                            "tracking_number" => "----",
                            "object" => "shipping_line",
                            "id" => "",
                            "parent_id" => ""
                        )
                    ),
                    "shipping_contact" => array(
                        "id" => "",
                        "object" => "shipping_contact",
                        "created_at" => "",
                        "parent_id" => "",
                        "receiver" => $app_user->name,
                        "phone" => $app_user->phone,
                        "between_streets" => $address_to_send->address,
                        "address"=> array(
                            "street1" => $address_to_send->address,
                            "city" => $address_to_send->city,
                            "state" => $address_to_send->state,
                            "postal_code" => $address_to_send->cp,
                            "country" => "mx",
                            "residential" => true,
                            "object" => "shipping_address"
                        )
                    )
                )
            );


            $response["error"] = false;
            $response["message"] = "";
            $response["order"] = $order;

        } catch (\Conekta\ProcessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function make_payment_spei($concepto, $cantidad_cobrar, $app_user, $address_to_send){

        $cantidad_cobrar = ($cantidad_cobrar * 100);
        $cantidad_cobrar = explode(".", $cantidad_cobrar);

        $response = array();
        $response["error"] = false;
        $response["message"] = "";
        $response["customer"] = "";

        \Conekta\Conekta::setApiKey(env("CONEKTA_PRIVATE_KEY"));
        \Conekta\Conekta::setApiVersion("2.0.0");

        try{
            $order = \Conekta\Order::create(
                array(
                    "line_items" => array(
                        array(
                            "name" => $concepto,
                            "unit_price" => $cantidad_cobrar[0],
                            "quantity" => 1
                        )
                    ),
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $app_user->customer_id
                    ), //customer_info
                    "charges" => array(
                        array(
                            "payment_method" => array(
                                "type" => "spei"
                            )
                        )
                    ),
                    "shipping_lines" => array(
                        array(
                            "amount" => 0,
                            "carrier" => "Fedex",
                            "method" => "Terrestre",
                            "tracking_number" => "----",
                            "object" => "shipping_line",
                            "id" => "",
                            "parent_id" => ""
                        )
                    ),
                    "shipping_contact" => array(
                        "id" => "",
                        "object" => "shipping_contact",
                        "created_at" => "",
                        "parent_id" => "",
                        "receiver" => $app_user->name,
                        "phone" => $app_user->phone,
                        "between_streets" => $address_to_send->address,
                        "address"=> array(
                            "street1" => $address_to_send->address,
                            "city" => $address_to_send->city,
                            "state" => $address_to_send->state,
                            "postal_code" => $address_to_send->cp,
                            "country" => "mx",
                            "residential" => true,
                            "object" => "shipping_address"
                        )
                    )
                )
            );


            $response["error"] = false;
            $response["message"] = "";
            $response["order"] = $order;

        } catch (\Conekta\ProcessingError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\ParameterValidationError $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        } catch (\Conekta\Handler $error){
            $response["error"] = true;
            $response["message"] = $error->message;
        }

        return $response;
    }


    public static function make_sku(){

        $reservation = null;
        $sku = "";

        do{

            $sku = chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90))."-".rand(100, 999);
            $reservation = Order::where("order_id", $sku)->count();

        }while( $reservation >= 1 );

        return $sku;
    }








    public static function sendPushNotification($notification){


        $API_ACCESS_KEY = env('SERVER_KEY_FIREBASE');

        $singleID = $notification->RESPONSE["UUID"];

        $instructions = array();
        $instructions["TEXTO_NOTIFICACION"] = $notification->RESPONSE["TEXTO_NOTIFICACION"];

        $fcmMsg = array(
            'body'          => $notification->RESPONSE["TEXTO_NOTIFICACION"],
            'title'         => $notification->RESPONSE["TITLE"],
            'subtitle'      => "Recsol",
            'sound'         => "default",
        );

        $fcmFields = array(
            'to' => $singleID,
            'priority' => 'high',
            'data'  => $instructions,
            'notification' => $fcmMsg
        );

        $headers = array(
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        $result = json_decode($result);

        return $result;
    }


    public static function autocomplete_array($obj, $colum){

        $obj = $obj->groupBy($colum)->get()->toArray();
        $tags = "";
        foreach ($obj as $item){
            $tags .= $item[$colum].",";
        }

        return $tags;
    }


    public static function format_product_from_carrito($product){

        $product_formated = array();
        $price = array();
        $price["maria_luisa_precio_original"] = 0;
        $price["maria_luisa_descuento"] = 0;
        $price["maria_luisa_precio_final"] = 0;

        $price["vidrio_precio_original"] = 0;
        $price["vidrio_descuento"] = 0;
        $price["vidrio_precio_final"] = 0;

        $price["extra_precio_original"] = 0;
        $price["extra_descuento"] = 0;
        $price["extra_precio_final"] = 0;

        $price["frame_precio_original"] = 0;
        $price["frame_descuento"] = 0;
        $price["frame_precio_final"] = 0;

        $price["unique_price"] = 0;

        $price["gran_total"] = 0;

        if($product["frame_type"] == "titulo"){

            $degree_frame_configurations = DegreeFrameConfiguration::where("id", intval($product["configuration_id"]))->first();
            $degree_frame = DegreeFrame::where("id", intval($product["frame_id"]))->first();
            $glass = Glass::where("id", intval($product["glass_id"]))->first();

            if($product["extra_id"] != ""){
                $extra = Extra::find(intval($product["extra_id"]))->first();
            }else{
                $extra = null;
            }

            $product_formated["rowid"] = $product["rowid"];
            $product_formated["frame"] = $degree_frame->name;
            $product_formated["tipo"] = "Titulo";
            $product_formated["position"] = ($product["position"] == "horizontal") ? "Horizontal" : "Vertical";
            $product_formated["quantity"] = intval($product["quantity"]);
            $product_formated["color"] = $degree_frame_configurations->get_color["name"];
            $product_formated["height"] = $degree_frame->height;
            $product_formated["width"] = $degree_frame->width;
            $product_formated["height_finish"] = $degree_frame->height_finish;
            $product_formated["width_finish"] = $degree_frame->width_finish;


            if($degree_frame_configurations->image_url_with_maria_luisa == null){
                $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_color;
                $product_formated["tipo_maria_luisa"] = "Sin María Luisa";
            }else{
                $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_maria_luisa;
                $product_formated["tipo_maria_luisa"] = $degree_frame_configurations->get_maria_luisa["name"];
            }

            $product_formated["tipo_vidrio"] = $glass->name;


            if($extra != null){
                $product_formated["extra"] = $extra->name;
            }else{
                $product_formated["extra"] = "";
            }


            //Calculamos el precio
            //Precio Maria Luisa
            if( $degree_frame_configurations->maria_luisa_id != null){
                $price["maria_luisa_precio_original"] = $degree_frame_configurations->get_maria_luisa["price"];
                $price["maria_luisa_descuento"] = $degree_frame_configurations->get_maria_luisa["descount"];
                $price["maria_luisa_precio_final"] = $degree_frame_configurations->get_maria_luisa["price"] - $degree_frame_configurations->get_maria_luisa["descount"];
            }

            //Precio vidrio
            if( $glass != null ){
                $price["vidrio_precio_original"] = $glass->price;
                $price["vidrio_descuento"] = $glass->descount;
                $price["vidrio_precio_final"] = $glass->price - $glass->descount;
            }

            //Precio extra
            if( $extra != null ){
                $price["extra_precio_original"] = $extra->price;
                $price["extra_descuento"] = $extra->descount;
                $price["extra_precio_final"] = $extra->price - $glass->descount;
            }

            //Precio extra
            if( $degree_frame != null ){
                $price["frame_precio_original"] = $degree_frame->price;
                $price["frame_descuento"] = $degree_frame->discount;
                $price["frame_precio_final"] = $degree_frame->price - $degree_frame->discount;
            }

            $price["unique_price"] = $price["maria_luisa_precio_final"] + $price["vidrio_precio_final"] + $price["extra_precio_final"] + $price["frame_precio_final"];

            $price["gran_total"] = $price["unique_price"] * $product["quantity"];

        }
        else if($product["frame_type"] == "diploma"){

            $degree_frame_configurations = DiplomaFrameConfiguration::where("id", intval($product["configuration_id"]))->first();
            $degree_frame = DiplomaFrame::where("id", intval($product["frame_id"]))->first();
            $glass = Glass::where("id", intval($product["glass_id"]))->first();

            if($product["extra_id"] != ""){
                $extra = Extra::find(intval($product["extra_id"]))->first();
            }else{
                $extra = null;
            }

            $product_formated["rowid"] = $product["rowid"];
            $product_formated["frame"] = $degree_frame->name;
            $product_formated["tipo"] = "Diploma";
            $product_formated["position"] = ($product["position"] == "horizontal") ? "Horizontal" : "Vertical";
            $product_formated["quantity"] = intval($product["quantity"]);
            $product_formated["color"] = $degree_frame_configurations->get_color["name"];
            $product_formated["height"] = $degree_frame->height;
            $product_formated["width"] = $degree_frame->width;


            if($product["position"] == "horizontal"){
                if($degree_frame_configurations->image_url_with_maria_luisa != null){
                    $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_maria_luisa;
                    $product_formated["tipo_maria_luisa"] = $degree_frame_configurations->get_maria_luisa["name"];
                }else{
                    $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_color;
                    $product_formated["tipo_maria_luisa"] = "Sin María Luisa";
                }
            }else{
                if($degree_frame_configurations->image_url_with_maria_luisa_vertical != null){
                    $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_maria_luisa_vertical;
                    $product_formated["tipo_maria_luisa"] = $degree_frame_configurations->get_maria_luisa["name"];
                }else{
                    $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_color_vertical;
                    $product_formated["tipo_maria_luisa"] = "Sin María Luisa";
                }
            }

            $product_formated["tipo_vidrio"] = $glass->name;

            if($extra != null){
                $product_formated["extra"] = $extra->name;
            }else{
                $product_formated["extra"] = "";
            }

            //Calculamos el precio
            //Precio Maria Luisa
            if( $degree_frame_configurations->maria_luisa_id != null){
                $price["maria_luisa_precio_original"] = $degree_frame_configurations->get_maria_luisa["price"];
                $price["maria_luisa_descuento"] = $degree_frame_configurations->get_maria_luisa["descount"];
                $price["maria_luisa_precio_final"] = $degree_frame_configurations->get_maria_luisa["price"] - $degree_frame_configurations->get_maria_luisa["descount"];
            }

            //Precio vidrio
            if( $glass != null ){
                $price["vidrio_precio_original"] = $glass->price;
                $price["vidrio_descuento"] = $glass->descount;
                $price["vidrio_precio_final"] = $glass->price - $glass->descount;
            }

            //Precio extra
            if( $extra != null ){
                $price["extra_precio_original"] = $extra->price;
                $price["extra_descuento"] = $extra->descount;
                $price["extra_precio_final"] = $extra->price - $glass->descount;
            }

            //Precio extra
            if( $degree_frame != null ){
                $price["frame_precio_original"] = $degree_frame->price;
                $price["frame_descuento"] = $degree_frame->discount;
                $price["frame_precio_final"] = $degree_frame->price - $degree_frame->discount;
            }

            $price["unique_price"] = $price["maria_luisa_precio_final"] + $price["vidrio_precio_final"] + $price["extra_precio_final"] + $price["frame_precio_final"];

            $price["gran_total"] = $price["unique_price"] * $product["quantity"];

        }
        else if($product["frame_type"] == "portaretratos"){

            $degree_frame_configurations = PortaretratoFrameConfiguration::where("id", intval($product["configuration_id"]))->first();
            $degree_frame = PortaretratoFrame::where("id", intval($product["frame_id"]))->first();
            //$glass = Glass::where("id", intval($product["glass_id"]))->first();

            $extra = null;

            $product_formated["rowid"] = $product["rowid"];
            $product_formated["frame"] = $degree_frame->name;
            $product_formated["tipo"] = "Portaretrato";
            $product_formated["position"] = ($product["position"] == "horizontal") ? "Horizontal" : "Vertical";
            $product_formated["quantity"] = intval($product["quantity"]);
            $product_formated["color"] = "";
            $product_formated["height"] = $degree_frame->height;
            $product_formated["width"] = $degree_frame->width;


            if($degree_frame_configurations->image_url_with_maria_luisa == null){
                $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_color;
                $product_formated["tipo_maria_luisa"] = "Sin María Luisa";
            }else{
                $product_formated["image_frame"] = $degree_frame_configurations->image_url_with_maria_luisa;
                $product_formated["tipo_maria_luisa"] = $degree_frame_configurations->get_maria_luisa["name"];
            }

            $product_formated["tipo_vidrio"] = "";
            $product_formated["extra"] = "";


            //Calculamos el precio
            //Precio Maria Luisa
            if( $degree_frame_configurations->maria_luisa_id != 0){
                $price["maria_luisa_precio_original"] = $degree_frame_configurations->get_maria_luisa["price"];
                $price["maria_luisa_descuento"] = $degree_frame_configurations->get_maria_luisa["descount"];
                $price["maria_luisa_precio_final"] = $degree_frame_configurations->get_maria_luisa["price"] - $degree_frame_configurations->get_maria_luisa["descount"];
            }

            //Precio vidrio **No lleva

            //Precio extra **No lleva

            //Precio marco
            if( $degree_frame != null ){
                $price["frame_precio_original"] = $degree_frame->price;
                $price["frame_descuento"] = $degree_frame->discount;
                $price["frame_precio_final"] = $degree_frame->price - $degree_frame->discount;
            }

            $price["unique_price"] = $price["maria_luisa_precio_final"] + $price["vidrio_precio_final"] + $price["extra_precio_final"] + $price["frame_precio_final"];

            $price["gran_total"] = $price["unique_price"] * $product["quantity"];
        }

        $product_formated["prices"] = $price;

        return $product_formated;
    }


    public static function shipping_cost($products){

        $quantity_frames = 0;
        $total_packages = 0;
        foreach ($products as $product){
            $quantity_frames += $product["quantity"];
        }

        //Cada envio es por 3 marcos
        $total_packages = intval($quantity_frames / 3);

        //En caso de que la cantidad de marcos no sea multiplo de 3
        $total_packages += (($quantity_frames % 3) > 0 ) ? 1 : 0;

        return ($total_packages * 100);
    }


}

































