<?php

namespace MetodikaTI\Http\Controllers\Back;

use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\Http\Requests\Back\Account\LoginRequest;
use MetodikaTI\Http\Requests\Back\Account\ResetPasswordRequest;
use MetodikaTI\Library\Pastora;
use MetodikaTI\PasswordReset;
use MetodikaTI\PasswordResets;
use MetodikaTI\User;
use Session;
use Illuminate\Support\Facades\Mail;


class AccountController extends Controller
{
    use ResetsPasswords;
    protected $tokens;


    public function __construct(Guard $auth, PasswordBroker $passwords)
    {
        $this->auth = $auth;
        $this->passwords = $passwords;
        $this->subject = 'Recuperación de contraseña';
    }


    public function getHome()
    {
        return view('back.account.home');
    }


    public function postLogin(LoginRequest $request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {

            return redirect()->intended('dashboard');

        } else {
            Session::flash('loginError', true);
            Session::flash('lastEmail', $request->email);
            Session::flash('loginMsg', 'Cuenta de correo y/o contraseña incorrectos.');
            return redirect()->intended('');
        }

    }


    protected function guard()
    {
        return Auth::guard('clients');
    }


    public function getLogout()
    {
        Auth::logout();

        return redirect("");
    }


    public function getRecovery()
    {
        return view('back.account.recovery');
    }


    public function postReset(Request $request)
    {

        $response = array();
        $response["status"] = false;
        $response["message"] = "";

        $user = User::where("email", $request->email)->first();

        if ($user != null) {

            $password_reset = PasswordReset::where("email", $user->email);
            if ($password_reset->count() > 0) {
                $password_reset = $password_reset->first();
            } else {
                $password_reset = new PasswordReset();
            }

            $password_reset->email = $user->email;
            $password_reset->token = base64_encode(date("d-m-Y H:i:s"));
            $password_reset->save();

            \Mail::send("mail.recover_password", ["user" => $user, "token" => $password_reset->token], function ($message) use ($user) {
                $message->to($user->email)->subject('Recuperación de contraseña');
            });

            $response["status"] = true;
            $response["message"] = "Te hemos enviado un correo electrónico con instrucciones para reestablecer tu contraseña.";
            $response["url"] = url("");

        } else {
            $response["status"] = false;
            $response["message"] = "El correo electrónico no se encuentra registrado, intentalo nuevamente.";
        }

        return response()->json($response);
    }


    public function change_password($token, Request $request)
    {

        $error = true;

        //Rectificamos que el token exista y que la fecha de creacion no sea mayor a 30 minutos
        $password_reset = PasswordReset::where("token", $request->token)->first();

        if ($password_reset != null) {

            $password_reset_time = Carbon::createFromFormat("Y-m-d H:i:s", $password_reset->updated_at);
            $now = Carbon::now();
            if ($now->diffInMinutes($password_reset_time) < 30) {
                $error = false;
            } else {
                $error = true;
            }

        } else {
            $error = true;
        }


        return view('back.account.recovery', ["token" => $token, "error" => $error]);
    }


    public function update_password($token, Request $request)
    {

        $response = array();
        $response["error"] = true;
        $response["message"] = "";

        $password_reset = PasswordReset::where("token", $request->token)->first();
        if ($password_reset != null) {

            if ($request->password != $request->password_confirmed) {
                $response["error"] = true;
                $response["message"] = "El campo contraseña y confirmar contraseña no coinciden.";
            } else {

                $users = User::where("email", $password_reset->email)->first();

                if ($users != null) {

                    $users->password = bcrypt($request->password);
                    $users->save();

                    PasswordReset::where("token", $request->token)->delete();

                    $response["error"] = false;
                    $response["message"] = "La contraseña se ha restablecido correctamente.";
                    $response["url"] = url("/");
                } else {
                    $response["error"] = true;
                    $response["message"] = "No hemos podido restablecer tu contraseña, solicita la recuperación de contraseña nuevamente.";
                }

            }

        } else {
            $response["error"] = true;
            $response["message"] = "No hemos podido restablecer tu contraseña, solicita la recuperación de contraseña nuevamente.";
        }

        return response()->json($response);
    }


}











