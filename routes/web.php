<?php

Route::get('test', 'Back\AccountController@test');

Route::get('/', 'Back\AccountController@getHome')->name('login');
Route::get('change_password/{token}', 'Back\AccountController@change_password');
Route::post('update_password/{token}', 'Back\AccountController@update_password')->name("update_password");


Route::group(['prefix' => 'account'], function () {

    Route::get('logout', 'Back\AccountController@getLogout')->name('admin_logout');
    Route::post('login', 'Back\AccountController@postLogin')->name('admin_login');
    Route::post('password/reset', 'Back\AccountController@postReset')->name('password.reset');
});

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'dashboard'], function () {


        Route::get('/', 'Back\Dashboard\DashboardController@index');


        Route::group(['prefix' => 'system'], function () {


            Route::group(['prefix' => 'user'], function () {
                Route::get('/', 'Back\Dashboard\System\UserController@index')->name('user_index');
                Route::get('create', 'Back\Dashboard\System\UserController@create')->name('user_create_get');
                Route::post('store', 'Back\Dashboard\System\UserController@store')->name('user_create');
                Route::get('edit/{id}', 'Back\Dashboard\System\UserController@edit')->name('user_edit');
                Route::post('update/{id}', 'Back\Dashboard\System\UserController@update')->name('user_update');
                Route::get('delete/{id}', 'Back\Dashboard\System\UserController@destroy')->name('user_delete');

                Route::get('edit_unique/{id}', 'Back\Dashboard\System\UserController@edit_unique')->name('user_unique_edit');
                Route::post('update_unique/{id}', 'Back\Dashboard\System\UserController@update_unique')->name('user_unique_update');

            });


            Route::group(['prefix' => 'profile'], function () {
                Route::get('/', 'Back\Dashboard\System\ProfileController@index')->name('profile_index');
                Route::get('create', 'Back\Dashboard\System\ProfileController@create')->name('profile_create');
                Route::post('store', 'Back\Dashboard\System\ProfileController@store')->name('profile.store');
                Route::get('edit/{id}', 'Back\Dashboard\System\ProfileController@edit')->name('profile.edit');
                Route::post('update', 'Back\Dashboard\System\ProfileController@update')->name('profile.update');
                Route::get('getData', 'Back\Dashboard\System\ProfileController@getData')->name('profile.data');
                Route::get('view/{id}', 'Back\Dashboard\System\ProfileController@show')->name('profile.view');
                Route::get('delete/{id}', 'Back\Dashboard\System\ProfileController@destroy')->name('profile.delete');
            });
        });


        Route::group(['prefix' => 'catalogos'], function () {

            Route::group(['prefix' => 'colors'], function () {
                Route::get('/', 'Back\Catalog\ColorController@index')->name('colors_index');
                Route::get('create', 'Back\Catalog\ColorController@create')->name('colors_create');
                Route::post('store', 'Back\Catalog\ColorController@store')->name('colors.store');
                Route::get('edit/{id}', 'Back\Catalog\ColorController@edit')->name('colors.edit');
                Route::post('update/{id}', 'Back\Catalog\ColorController@update')->name('colors.update');
                Route::get('delete/{id}', 'Back\Catalog\ColorController@destroy')->name('colors.delete');
            });

        });

    });


});
