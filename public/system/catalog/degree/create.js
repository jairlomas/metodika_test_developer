$(document).ready(function () {


    $('#glass_select, #university_select').multiSelect({
        selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
        selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder=''>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });


    $(this).on("click", ".add_new_configuration", function (e) {
        e.preventDefault();

        var url_source = $(this).attr("route");

        $.ajax({
            url: url_source,
            type: "GET",
            dataType: "text",
            error: function (e) {
                console(e);
            },
            success: function (result) {
                console.log(result);
                $("#dinamyc_configuration").append(result);

                $("select[custom-select='select2']").select2();

            }
        });

    });


    $(this).on("click", ".remove_row_configuration", function (e) {
        e.preventDefault();

        $(this).parent().parent().parent().remove();

    });


    $(this).on("click", ".remove_row_configuration_data_base", function (e) {
        e.preventDefault();

        var url_source = $(this).attr("route");
        var dom = $(this);


        Swal.fire({
            title: 'Mensaje',
            text: '¿Deseas eliminar la configuración del marco seleccionado?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    url: url_source,
                    type: "GET",
                    dataType: "JSON",
                    error: function (e) {
                        console(e);
                    },
                    success: function (result) {
                        console.log(result);

                        dom.parent().parent().parent().remove();

                        swal({
                            title: "¡Excelente!",
                            text: result.message,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonText: "Aceptar",
                        }).then((willDelete) => {
                        });

                    }
                });

            } else if (result.dismiss === Swal.DismissReason.cancel) {

            }
        });


    });


});


 