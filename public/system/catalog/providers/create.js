$(document).ready(function () {

    $(this).on("click", ".add-new-comment", function (e) {

        var area_text = "";
        area_text +=  '<div class="form-group">';
        area_text +=  '     <div class="row">';
        area_text +=  '         <div class="col-md-10">';
        area_text +=  '             <textarea class="form-control" name="nota[]" rows="4"></textarea>';
        area_text +=  '         </div>';
        area_text +=  '         <div class="col-md-2">';
        area_text +=  '             <div class="btn btn-danger" onclick="$(this).parent().parent().parent().remove();"><i class="fas fa-minus-circle"></i> Eliminar</div>';
        area_text +=  '         </div>';
        area_text +=  '     </div>';
        area_text +=  '</div>';

        $("#notas-cliente").append(area_text);

    });



    $(this).on("click", ".add-new-attach", function (e) {

        var area_text = "";
        area_text +=  '<div class="form-group">';
        area_text +=  '     <div class="row">';
        area_text +=  '         <div class="col-md-10">';
        area_text +=  '             <input name="attach[]" class="form-control" type="file">';
        area_text +=  '         </div>';
        area_text +=  '         <div class="col-md-2">';
        area_text +=  '             <div class="btn btn-danger" onclick="$(this).parent().parent().parent().remove();"><i class="fas fa-minus-circle"></i> Eliminar</div>';
        area_text +=  '         </div>';
        area_text +=  '     </div>';
        area_text +=  '</div>';

        $("#adjuntos-cliente").append(area_text);

    });







});