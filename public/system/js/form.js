$(document).ready(function () {

    $(this).on("keypress", "form", function (event) {
        if (event.target.localName != "textarea") {
            return event.keyCode != 13;
        }
    });

    $('form', this).on('submit', function (e) {
        e.preventDefault();

        var data = new FormData(this);

        // if( $('#fecha_disponibilidad').length > 0 ){
        //     data.append("fecha_disponibilidad", $('#fecha_disponibilidad').multiDatesPicker('getDates'));
        // }

        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: data,
            beforeSend: function () {
                blockForm();
            },
            error: function (e) {

                unblockForm();
                if (e.status == 422) {
                    modalErrors(JSON.parse(e.responseText));
                } else {
                    errorAlert('!Ups, algo salio mal!');
                }
            },
            success: function (response) {

                unblockForm();
                if (response.status == true) {

                    swal({
                        title: "¡Excelente!",
                        text: response.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                    }).then((willDelete) => {
                        window.location = response.url;
                    });

                } else {
                    errorAlert(response.message);
                }
            }
        });

    });


    $("select[custom-select='select2-tags']").select2({
        tags: true
    });


    $("select[custom-select='select2']").select2();


    if( $("input[custom-input='autocomplete']").length > 0){
        $("input[custom-input='autocomplete']").each(function (i, obj) {

            var obj = $(obj);
            var input_autocomplete = (obj.attr("available-tags")).split(",");

            obj.autocomplete({
                source: input_autocomplete,
            });

        });
    }


    if( $("input[custom-input='date-input']").length > 0){
        $("input[custom-input='date-input']").each(function (i, obj) {

            //$( "#datepicker" ).datepicker();
            var obj = $(obj);
            obj.datepicker();

        });
    }


});


