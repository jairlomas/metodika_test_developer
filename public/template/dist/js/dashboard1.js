

// Dashboard 1 Morris-chart



var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];


Morris.Area({
        element: 'morris-area-chart2',
        data: [{
            period: '2019-01',
            Postulaciones: 56,

        }, {
            period: '2019-02',
            Postulaciones: 100,

        }, {
            period: '2019-03',
            Postulaciones: 62,

        }, {
            period: '2019-04',
            Postulaciones: 41,

        }, {
            period: '2019-05',
            Postulaciones: 23,

        }, {
            period: '2019-06',
            Postulaciones: 20,

        },
         {
             period: '2019-07',
             Postulaciones: 55,

        }],
        xkey: 'period',
        ykeys: ['Postulaciones'],
        labels: ['Postulaciones'],
        pointSize: 0,
        fillOpacity: 0.4,
        pointStrokeColors:['#01c0c8'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 0,
        smooth: true,
        hideHover: 'auto',
        lineColors: ['#01c0c8'],
        resize: true,
        xLabelFormat : function (x) {
            console.log(x.getMonth());
            return months[x.getMonth()];
        }
    });

 // Morris donut chart
        
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Informatica",
            value: 8500,

        }, {
            label: "Servicio al cliente",
            value: 3630,
        }, {
            label: "Carpintería",
            value: 4870
        }],
        resize: true,
        colors:['#fb9678', '#01c0c8', '#4F5467']
    });
 
$("#sparkline8").sparkline([2,4,4,6,8,5,6,4,8,6,6,2 ], {
            type: 'line',
            width: '100%',
            height: '130',
            lineColor: '#00c292',
            fillColor: 'rgba(0, 194, 146, 0.2)',
            maxSpotColor: '#00c292',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#00c292'
        });
        $("#sparkline9").sparkline([0,2,8,6,8,5,6,4,8,6,6,2 ], {
            type: 'line',
            width: '100%',
            height: '130',
            lineColor: '#03a9f3',
            fillColor: 'rgba(3, 169, 243, 0.2)',
            minSpotColor:'#03a9f3',
            maxSpotColor: '#03a9f3',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#03a9f3'
        });
        $("#sparkline10").sparkline([2,4,4,6,8,5,6,4,8,6,6,2], {
            type: 'line',
            width: '100%',
            height: '130',
            lineColor: '#fb9678',
            fillColor: 'rgba(251, 150, 120, 0.2)',
            maxSpotColor: '#fb9678',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#fb9678'
        });
